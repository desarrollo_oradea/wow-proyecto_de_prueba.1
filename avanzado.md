# Enlaces e imágenes con markdown

## enlaces

### enlaces externos

https://www.google.com/
[Página de google](https://www.google.com/)

### enlaces internos

[Documento de interés](prueba de texto.md)

## imagenes

### Inline style

![Foto que me gustaba](foto_prueba_borrar.jpg)

### Reference style

![Foto que me gusta][logo]

[logo]: foto_prueba_borrar.jpg

```html
<html>
esto se renderiza como código fuente en lenguaje html
</html>
```

> Asereje de je dejebere. Pablo Cohelo

@jmmm
@all

:poop:
